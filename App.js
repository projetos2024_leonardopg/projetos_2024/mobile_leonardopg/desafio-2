import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CountMoviment from "./src/countMovement";
import ImageMovement from "./src/imageMovement";
import HomeScreen from "./src/home";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="ImageMovement" component={ImageMovement} />
        <Stack.Screen name="CountMovement" component={CountMoviment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
