import React, { useRef, useState, useEffect } from "react";
import {
  View,
  Dimensions,
  PanResponder,
  Button,
  Text,
  Image,
} from "react-native";

const imageMovement = () => {
  const [count, setCount] = useState(0);
  const [imageIndex, setImageIndex] = useState(0);
  const images = [
    require("../assets/unnamed (1).jpg"),
    require("../assets/23615ce774a4264fc334b1a5cd422de4.jpg"),
    require("../assets/images (1).jpg"),
    require("../assets/unnamed.jpg"),
    require("../assets/barbas-estranhas-3.jpg"),
    require("../assets/o-mc-guime-do-bbb-imitando-o-goku-careca-t3ddy-cobre-os-v0-3mxixdrnbkna1.jpg")
  ];

  const screenWidth = Dimensions.get("window").width;
  const gestureThreshold = screenWidth * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (Math.abs(gestureState.dx) > gestureThreshold) {
          setCount((prevConut) => prevConut + 1);
          if (gestureState.dx > 0) {
            setImageIndex((prevIndex) =>
              prevIndex === images.length - 1 ? 0 : prevIndex + 1
            );
          } else {
            setImageIndex((prevIndex) =>
              prevIndex === 0 ? images.length - 1 : prevIndex - 1
            );
          }
        }
      },
    })
  ).current;

  return (
    <View
      {...panResponder.panHandlers}
      style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
    >
      <Image source={images[imageIndex]} style={{ width: 200, height: 200 }} />
      <Text>Valor do Contador: {count}</Text>
    </View>
  );
};

export default imageMovement;
