import React, { useRef, useState, useEffect } from "react";
import { View, Dimensions, PanResponder, Button, Text } from "react-native";

const CountMoviment = () => {
  const [count, setCount] = useState(0);
  const screenHeight = Dimensions.get("window").height;
  const gestureThreshole = screenHeight * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < -gestureThreshole) {
          setCount((prevConut) => prevConut + 1);
        }
      },
    })
  ).current;

  return (
    <View
      {...panResponder.panHandlers}
      style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
    >
      <Text>Valor do Contador: {count}</Text>
    </View>
  );
};

export default CountMoviment;
